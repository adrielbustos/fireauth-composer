<?php

namespace FireBack\FireAuth\Models;
use FireBack\FireAuth\Http\FireHttp;
use FireBack\FireAuth\Http\Response;

class Users extends FireHttp
{
    /**
     * @var int
     */
    public int $id;
    /**
     * @var string
     */
    public string $email;
    /**
     * @var string
     */
    public string $password;
    /**
     * @var bool
     */
    public bool $active;
    /**
     * @var string
     */
    public string $name;
    /**
     * @var array<UserSocialNetwork>
     */
    public array $user_socialNetwork = [];
    /**
     * @var array<TokenPayments>
     */
    public array $tokenPayments = [];

    /**
     * Users constructor.
     * @param string $email
     * @param string $password
     * @param bool $active
     */
    public function __construct (int $id = 0)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId (): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId (int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getEmail (): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail (string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword (): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword (string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return bool
     */
    public function isActive (): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive (bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return string
     */
    public function getName (): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName (string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return UserSocialNetwork[]
     */
    public function getSocialNetwork (): array
    {
        return $this->user_socialNetwork;
    }

    /**
     * @param UserSocialNetwork[] $socialNetwork
     */
    public function setSocialNetwork (array $socialNetwork): void
    {
        $this->user_socialNetwork = $socialNetwork;
    }

    /**
     * @param UserSocialNetwork $socialNetwork
     */
    public function addSocialNetwork(UserSocialNetwork $socialNetwork): void
    {
        $this->user_socialNetwork[] = $socialNetwork;
    }

    /**
     * @return TokenPayments[]
     */
    public function getTokenPayments (): array
    {
        return $this->tokenPayments;
    }

    public function addTokenPayments(TokenPayments $tokenPayments): void
    {
        $this->tokenPayments[] = $tokenPayments;
    }

    /**
     * @param TokenPayments[] $tokenPayments
     */
    public function setTokenPayments (array $tokenPayments): void
    {
        $this->tokenPayments = $tokenPayments;
    }

    /**
     * @return Response
     */
    public function register(): Response
    {
        return $this->getRequest("/users/register", "post", $this);
    }

    /**
     * @return Response
     */
    public function login(): Response
    {
        return $this->getRequest("/users/login", "post", $this);
    }

    /**
     * @return Response
     */
    public function edit(): Response
    {
        return $this->getRequest("/users/update/{$this->id}", "put", $this);
    }

    /**
     * @return Response
     */
    public function delete(): Response
    {
        return $this->getRequest("/users/delete/{$this->id}", "delete", $this);
    }

    /**
     * @return Response
     */
    public function getInfo(): Response
    {
        return $this->getRequest("/users/get/{$this->id}");
    }

    /**
     * @return Response
     */
    public function getWhere(): Response
    {
        return $this->getRequest("/users/getWhere", "post", $this);
    }

}
