<?php
namespace FireBack\FireAuth\Models;
class TokenPayments
{
    /**
     * @var int
     */
    public int $id;
    /**
     * @var string
     */
    public string $accessToken;
    /**
     * @var bool
     */
    public bool $active;
    /**
     * @var PaymentType
     */
    public PaymentType $paymentType;
    /**
     * @var Users
     */
    public Users $users;

    /**
     * TokenPayments constructor.
     * @param string $accessToken
     * @param bool $active
     * @param PaymentType $paymentType
     * @param Users $users
     */
    public function __construct (string $accessToken, bool $active, PaymentType $paymentType, Users $users)
    {
        $this->accessToken = $accessToken;
        $this->active = $active;
        $this->paymentType = $paymentType;
        $this->users = $users;
    }

    /**
     * @return int
     */
    public function getId (): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId (int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getAccessToken (): string
    {
        return $this->accessToken;
    }

    /**
     * @param string $accessToken
     */
    public function setAccessToken (string $accessToken): void
    {
        $this->accessToken = $accessToken;
    }

    /**
     * @return bool
     */
    public function isActive (): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive (bool $active): void
    {
        $this->active = $active;
    }

    /**
     * @return PaymentType
     */
    public function getPaymentType (): PaymentType
    {
        return $this->paymentType;
    }

    /**
     * @param PaymentType $paymentType
     */
    public function setPaymentType (PaymentType $paymentType): void
    {
        $this->paymentType = $paymentType;
    }

    /**
     * @return Users
     */
    public function getUsers (): Users
    {
        return $this->users;
    }

    /**
     * @param Users $users
     */
    public function setUsers (Users $users): void
    {
        $this->users = $users;
    }

}
