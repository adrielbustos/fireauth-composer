<?php
namespace FireBack\FireAuth\Models;

class UserSocialNetwork
{
    /**
     * @var int
     */
    public int $id;
    /**
     * @var string
     */
    public string $email;
    /**
     * @var string
     */
    public string $profilePicture;
    /**
     * @var string
     */
    public string $tokenExpirationDate;
    /**
     * @var Users
     */
    public Users $users;
    /**
     * @var SocialNetwork
     */
    public SocialNetwork $socialNetwork;

    /**
     * UserSocialNetwork constructor.
     * @param string $email
     * @param Users $users
     * @param SocialNetwork $socialNetwork
     */
    public function __construct (Users $users, SocialNetwork $socialNetwork)
    {
        $this->email = $users->getEmail();
        $this->users = $users;
        $this->socialNetwork = $socialNetwork;
    }

    /**
     * @return int
     */
    public function getId (): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId (int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getEmail (): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail (string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getProfilePicture (): string
    {
        return $this->profilePicture;
    }

    /**
     * @param string $profilePicture
     */
    public function setProfilePicture (string $profilePicture): void
    {
        $this->profilePicture = $profilePicture;
    }

    /**
     * @return string
     */
    public function getTokenExpirationDate (): string
    {
        return $this->tokenExpirationDate;
    }

    /**
     * @param string $tokenExpirationDate
     */
    public function setTokenExpirationDate (string $tokenExpirationDate): void
    {
        $this->tokenExpirationDate = $tokenExpirationDate;
    }

    /**
     * @return Users
     */
    public function getUsers (): Users
    {
        return $this->users;
    }

    /**
     * @param Users $users
     */
    public function setUsers (Users $users): void
    {
        $this->users = $users;
    }

    /**
     * @return SocialNetwork
     */
    public function getSocialNetwork (): SocialNetwork
    {
        return $this->socialNetwork;
    }

    /**
     * @param SocialNetwork $socialNetwork
     */
    public function setSocialNetwork (SocialNetwork $socialNetwork): void
    {
        $this->socialNetwork = $socialNetwork;
    }

}
