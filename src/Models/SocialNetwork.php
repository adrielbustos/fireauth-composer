<?php
namespace FireBack\FireAuth\Models;
class SocialNetwork
{
    /**
     * @var int
     */
    public int $id;
    /**
     * @var string
     */
    public string $name;

    /**
     * @var array<UserSocialNetwork>
     */
    public array $user_socialNetwork = [];

    /**
     * SocialNetworks constructor.
     * @param int $id
     * @param string $name
     */
    public function __construct (int $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getId (): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId (int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName (): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName (string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return UserSocialNetwork[]
     */
    public function getUserSocialNetwork (): array
    {
        return $this->user_socialNetwork;
    }

    /**
     * @param UserSocialNetwork[] $user_socialNetwork
     */
    public function setUserSocialNetwork (array $user_socialNetwork): void
    {
        $this->user_socialNetwork = $user_socialNetwork;
    }

    /**
     * @param UserSocialNetwork $userSocialNetwork
     */
    public function addUserSocialNetwork(UserSocialNetwork $userSocialNetwork): void
    {
        $this->user_socialNetwork[] = $userSocialNetwork;
    }

}
