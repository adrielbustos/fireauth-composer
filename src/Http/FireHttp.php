<?php

namespace FireBack\FireAuth\Http;

abstract class FireHttp
{
    private const endPoint = 'http://c1741193.ferozo.com/auth/api/v1';
    private const token = 'leRaLqpuxSn+ZkR20gKTDGHulwipwU7yriHlmkQKgF8SgzTcBtvbgZCPkkvcHK4sl4uHphpaTiYT6A5c';
    private array $headers = ['Content-Type: application/json'];
    protected function getRequest(string $endpoint, string $protocol = 'get', object $data = null): Response
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::endPoint.$endpoint);
        if (!is_null($data)) {
            $data = json_encode($data);
        }
        switch (strtolower($protocol)) {
            case 'post':
                curl_setopt($ch, CURLOPT_POST, TRUE);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                break;
            case 'put':
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                break;
            case 'delete':
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
                break;
            default:
                break;
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->headers);
        $responsive = curl_exec($ch);
        if (!curl_errno($ch)) {
            switch ($http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE)) {
                case 200:  # OK
                    break;
                default:
                    throw new \Error("Error in ".self::endPoint.$endpoint);
            }
        }
        curl_close($ch);
        return self::_setResponse(json_decode($responsive));
    }
    private static function _setResponse(object $resp): Response
    {
        $response = new Response();
        foreach ($resp as $attr => $value)
        {
            if (property_exists($response, $attr))
            {
                $response->$attr = $value;
            }
        }
        return $response;
    }
}
