<?php

namespace FireBack\FireAuth\Http;

class Response
{
    /**
     * @var string
     */
    public string $firebackVersion = '';
    /**
     * @var bool
     */
    public bool $generalStatus = false;
    /**
     * @var bool
     */
    public bool $hasErrors = false;
    /**
     * @var int
     */
    public int $maxCount = 0;
    /**
     * @var mixed
     */
    public $data = [];
    /**
     * @var array
     */
    public array $errors = [];

    /**
     * Response constructor.
     */
    public function __construct ()
    {
    }

    /**
     * @return string
     */
    public function getFirebackVersion (): string
    {
        return $this->firebackVersion;
    }

    /**
     * @param string $firebackVersion
     */
    public function setFirebackVersion (string $firebackVersion): void
    {
        $this->firebackVersion = $firebackVersion;
    }

    /**
     * @return bool
     */
    public function isGeneralStatus (): bool
    {
        return $this->generalStatus;
    }

    /**
     * @param bool $generalStatus
     */
    public function setGeneralStatus (bool $generalStatus): void
    {
        $this->generalStatus = $generalStatus;
    }

    /**
     * @return bool
     */
    public function isHasErrors (): bool
    {
        return $this->hasErrors;
    }

    /**
     * @param bool $hasErrors
     */
    public function setHasErrors (bool $hasErrors): void
    {
        $this->hasErrors = $hasErrors;
    }

    /**
     * @return int
     */
    public function getMaxCount (): int
    {
        return $this->maxCount;
    }

    /**
     * @param int $maxCount
     */
    public function setMaxCount (int $maxCount): void
    {
        $this->maxCount = $maxCount;
    }

    /**
     * @return array
     */
    public function getData (): array
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData ($data): void
    {
        $this->data = $data;
    }

    /**
     * @return array
     */
    public function getErrors (): array
    {
        return $this->errors;
    }

    /**
     * @param array $errors
     */
    public function setErrors (array $errors): void
    {
        $this->errors = $errors;
    }

}
